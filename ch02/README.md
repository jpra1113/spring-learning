# Project Title

CH02. 

## Getting Started

學習建立各環境設定檔並印出

### Prerequisites

* 增加 HelloWorldController，呼叫取得各環境的變數

```java
@RestController
public class HelloWorldController {
	
	@Value("${env.name}")
	private String envName;

	@GetMapping("/hello")
	public String hello() {
		return "Hello world!! " + envName;
	}
}
```

* resources 目錄增加 application-ut.properties、application-uat.properties、application-prod.properties

* application-ut.properties 設定檔增加 env.name 變數

* application.properties 設定 spring.profiles.active變數來決定環境

* Run DemoApplication and open http://localhost:8080/hello
