# Project Title

CH03. 

## Getting Started

學習建立一個post API，並用postman工具呼叫取得下行

### Prerequisites

* 根據上行json，建立java相關物件

```json
{
    "header": {
        "msgId": "1",
        "txnSeq": "2",
        "branchId": "3",
        "clientIp": "4"
    },
    "body": {
        "customerId": "A123456789"
    }
}
```

* 增加 CustomerController，回應下行電文

```json
{
    "header": {
        "msgId": "1",
        "txnSeq": "2",
        "branchId": "3",
        "clientIp": "4"
    },
    "body": {
        "customerId": "A123456789",
        "name": "Alan",
        "age": 20,
        "tel": "0912345678",
        "addr": "地址"
    }
}
```