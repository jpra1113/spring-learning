# Project Title

CH07. 

## Getting Started

學習用 thymeleaf 框架(th:each、th:src、th:href)

### Prerequisites

* 加入static 資源文件設定

```
spring.resources.static-locations=classpath:/static/
```

* resources 增加 static目錄(css、js)
  * 增加 bootstrap.min.js
  * 增加 jquery.min.js
  * 增加 bootstrap.min.css
  
* Controller 增加 product 測試資料

```java
    @GetMapping("/prod")
    public String hello(Model model) {
        // 建立 Product 測試資料
        List<Product> dataList = new ArrayList<>();
        
        Product p1 = new Product();
        p1.setProductId(10);
        p1.setCode("10");
        p1.setName("產品10");
        p1.setUnitprice(BigDecimal.valueOf(12345678.456700));
        dataList.add(p1);
        
        Product p2 = new Product();
        p2.setProductId(20);
        p2.setCode("20");
        p2.setName("產品20");
        p2.setUnitprice(BigDecimal.valueOf(523236.214));
        dataList.add(p2);
        
        model.addAttribute("text", "This product page.");
        model.addAttribute("product", dataList);
        return "product";
    }
```

* product.html 使用 `th:each` 印出產品資料
