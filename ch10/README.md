# Project Title

CH10. 

## Getting Started

學習用 thymeleaf 框架(前端參數傳值到後端，後端呼叫composite層API取得資料)

### Prerequisites

學習用設定 `th:action`

```
<form class="form-horizontal" role="form" th:action="@{/prod/query}" method="post">
```

學習後端接收參數 `@RequestParam("productId")`

```java
public String queryProduct(Model model, @RequestParam("productId") Integer productId) {
```