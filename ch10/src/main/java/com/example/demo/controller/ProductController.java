package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dto.ProductResponse;
import com.example.demo.service.ProductService;
import com.example.demo.vo.Product;

@Controller
public class ProductController {
    
    @Autowired
    ProductService productService;

    @GetMapping("/prod")
    public String prod(Model model) {
        List<Product> dataList = new ArrayList<>();
        model.addAttribute("text", "This product page.");
        model.addAttribute("product", dataList);
        return "product";
    }

    @PostMapping("/prod/query")
    public String queryProduct(Model model, @RequestParam("productId") Integer productId) {
        // 建立 Product 測試資料
        List<Product> dataList = new ArrayList<>();
                
        ProductResponse productResponse = productService.findByProductId(productId);
        if (productResponse != null) {
            Product product = new Product();
            BeanUtils.copyProperties(productResponse.getBody(), product);
            dataList.add(product);
        }
        
        model.addAttribute("text", "This product page.");
        model.addAttribute("product", dataList);
        return "product";
    }
}
