package com.example.demo.service;

import com.example.demo.dto.ProductResponse;

public interface ProductService {
    
    ProductResponse findByProductId(Integer productId);

}
