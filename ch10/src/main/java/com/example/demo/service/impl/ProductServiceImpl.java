package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.CommonHeader;
import com.example.demo.dto.ProductRequest;
import com.example.demo.dto.ProductRequestBody;
import com.example.demo.dto.ProductResponse;
import com.example.demo.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
    
    @Value("${prod.url}")
    public String prodApiUrl;

    @Override
    public ProductResponse findByProductId(Integer productId) {
        ProductRequest request = new ProductRequest();
        
        CommonHeader header = new CommonHeader();
        request.setHeader(header);
        
        ProductRequestBody body = new ProductRequestBody();
        body.setProductId(productId);
        request.setBody(body);
        
        return new RestTemplate().postForObject(prodApiUrl, request, ProductResponse.class);
    }

}
