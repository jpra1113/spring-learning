# Project Title

CH08. 

## Getting Started

學習用 thymeleaf 框架(th:include)

### Prerequisites

* 加入 components 至 templates 目錄
* 把常用的html區塊 用 `th:fragment` 命名

```
<th:block th:fragment="productTable">
```

* 使用 `th:include` 把共用區塊載入

```
<th:block th:include="components/table :: productTable"></th:block>
```
