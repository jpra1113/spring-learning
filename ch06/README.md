# Project Title

CH06. 

## Getting Started

學習用 thymeleaf 框架(Hello World)

### Prerequisites

* pom.xml 加入 spring-boot-starter-thymeleaf

```xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
```

* resources 增加 templates(預設templates，若有特殊需求才需額外設定)

```
spring.thymeleaf.template-loader-path: classpath:/templates
spring.thymeleaf.suffix: .html
```

* 增加 HelloWorldController，把訊訊息文字返回至hello.html

```java
@Controller
public class HelloWorldController {

    @GetMapping("/hello")
    public String hello(Model model) {
        model.addAttribute("text", "Hello World!!");
        return "hello";
    }
}
```

* hello.html 需增加 `xmlns:th="http://www.thymeleaf.org"` 屬性
* 使用th:text 印出後端的傳遞的資料
