# Project Title

CH01. 

## Getting Started

學習建立spring-boot 專案，並建立一支hello API

### Prerequisites

* 增加 HelloWorldController，呼叫顯示'Hello world'

```java
@RestController
public class HelloWorldController {

	@GetMapping("/hello")
	public String hello() {
		return "Hello world";
	}
}
```

* SpringBootApplication 排除資料庫連線

```java
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
```

* Run DemoApplication and open http://localhost:8080/hello
