package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductSaveRequest {
	
	@JsonProperty("header")
	public CommonHeader header;
	
	@JsonProperty("body")
	public ProductSaveRequestBody body;

	public CommonHeader getHeader() {
		return header;
	}

	public void setHeader(CommonHeader header) {
		this.header = header;
	}

	public ProductSaveRequestBody getBody() {
		return body;
	}

	public void setBody(ProductSaveRequestBody body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "ProductRequest [header=" + header + ", body=" + body + "]";
	}
}
