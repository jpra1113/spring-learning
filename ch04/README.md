# Project Title

CH04. 

## Getting Started

學習spring data jpa，當API取得上行參數時，根據條件至資料庫取得資料

### Prerequisites

* 根據上行json，建立java相關物件

```json
{
    "header": {
        "msgId": "1",
        "txnSeq": "2",
        "branchId": "3",
        "clientIp": "4"
    },
    "body": {
        "productId": 10
    }
}
```

* 增加 ProductController，回應下行電文

```json
{
    "header": {
        "msgId": "1",
        "txnSeq": "2",
        "branchId": "3",
        "clientIp": "4"
    },
    "body": {
        "productId": 10,
        "code": "AB123",
        "name": "Leather Sofa",
        "unitprice": 1000
    }
}
```