package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.ProductRequest;
import com.example.demo.dto.ProductResponse;

@RestController
public class ProductController {
	
	@Value("${prod.url}")
	public String prodApiUrl;
	
	@PostMapping(value = "/prod", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ProductResponse prod(@RequestBody ProductRequest request) {
		return new RestTemplate().postForObject(prodApiUrl, request, ProductResponse.class);
	}
}
